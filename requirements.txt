Cython==0.27.3
numpy==1.15.2
scikit-learn==0.20.2
pandas==0.24.0
PyYAML==5.1.1

wheel==0.33.4
fastparquet==0.2.1
numba==0.48