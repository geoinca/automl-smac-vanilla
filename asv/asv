#!/usr/bin/env python3

import argparse
import logging

from asv.utils.optimize import optimize
from asv.utils.stats import Stats

def get_cli_args():
    parser = argparse.ArgumentParser(description='Run AutoML Smac Vanilla (ASV)')

    parser.add_argument(
        '--dataset',
        action='store',
        dest='dataset',
        default="covertype",
        type=str
    )

    parser.add_argument(
        '--dataset-mapper-file',
        action='store',
        dest='dataset_mapper_file',
        default="datasets.yaml",
        type=str
    )

    parser.add_argument(
        '--search-space',
        action='store',
        dest='search_space',
        default="classifiers.json",
        type=str
    )

    parser.add_argument(
        '--seed',
        action='store',
        dest='seed',
        default=None,
        type=int
    )

    parser.add_argument(
        '--cutoff-time',
        action='store',
        dest='cutoff_time',
        default=60*5,
        type=int
    )

    parser.add_argument(
        '--number-of-evaluations',
        action='store',
        dest='number_of_evaluations',
        default=10100,
        type=int
    )

    parser.add_argument(
        '--number-of-candidates-per-group',
        action='store',
        dest='number_of_candidates_per_group',
        default=100, # equivalent to number of individuals per gen in TPOT, useful to compute convergence
        type=int
    )

    parser.add_argument(
        '--train-size-ratio',
        action='store',
        dest='train_size_ratio',
        default=0.75,
        type=float
    )

    parser.add_argument(
        '--test-size-ratio',
        action='store',
        dest='test_size_ratio',
        default=0.25,
        type=float
    )

    parser.add_argument(
        '--stats-result-file',
        action='store',
        dest='stats_result_file',
        default="result.json",
        type=str
    )

    parser.add_argument(
        '--cross-validation',
        action='store',
        dest='cross_validation',
        default=5, # default TPOT value
        type=int
    )

    return parser.parse_args()

if __name__ == '__main__':
    #logging.getLogger('utils.optimize.SMBOWrapper').setLevel(logging.DEBUG)
    logging.getLogger().setLevel(logging.INFO)
    logging.basicConfig(format='%(asctime)s %(message)s')
    args = get_cli_args()
    Stats(number_of_candidates_per_group=args.number_of_candidates_per_group)
    optimize(args)
