import importlib
import numpy as np
import time

from sklearn.model_selection import cross_val_score
from sklearn.metrics import SCORERS

from smac.tae.execute_func import AbstractTAFunc
from smac.tae.execute_ta_run import StatusType
from smac.utils.constants import MAXINT

from ConfigSpace.configuration_space import ConfigurationSpace

from .config_space import get_hyperparameters
from .stats import Stats

class ExecuteTA(AbstractTAFunc):
    def __init__(self, stats=None, runhistory=None, run_obj="quality",
                 memory_limit=None, par_factor=1,
                 cost_for_crash=float(MAXINT),
                 abort_on_first_run_crash=False,
                 use_pynisher=True, raw_config_space=None,
                 X_train=None, y_train=None, X_test=None, y_test=None,
                 cross_validation=5):
         super().__init__(ta=self.evaluation, stats=stats,
                         runhistory=runhistory, run_obj=run_obj,
                         memory_limit=memory_limit, par_factor=par_factor,
                         cost_for_crash=cost_for_crash)

         self.X_train = X_train
         self.y_train = y_train
         self.X_test = X_test
         self.y_test = y_test
         self.raw_config_space = raw_config_space
         self.cross_validation = cross_validation

    def run(self, config, instance=None,
            cutoff=None,
            seed=12345,
            instance_specific="0"):

        status, cost, runtime, additional_run_info = super().run(config=config,
            instance=instance, cutoff=cutoff, seed=seed,
            instance_specific=instance_specific)

        score = np.nan if status == StatusType.TIMEOUT else cost

        if status == StatusType.SUCCESS:
            model = self.get_model(config)
            test_score, extra_test_time = self.evaluation_on_test(model)
        else:
            test_score = np.nan
            extra_test_time = 0

        Stats().append_candidate(name=str(config.get_dictionary()), score=score, test_score=test_score, status=str(status), extra_test_time=extra_test_time)

        return status, cost, runtime, additional_run_info

    def get_model(self, config):
        estimator = str(config.get('__estimator__'))
        backend = self.raw_config_space[estimator]['backend']
        class_identifier = self.raw_config_space[estimator]['class_identifier']

        if backend == 'sklearn':
            classifier_group = ".".join(class_identifier.split('.')[:-1])
            module_name = "sklearn.{}".format(classifier_group)
            module = importlib.import_module(module_name)
            class_name = class_identifier.split(".")[-1]
        else:
            raise Exception('Unexpected backend {}'.format(backend))

        estimator = getattr(module, class_name)

        model = estimator(**get_hyperparameters(config))

        return model

    def evaluation_on_test(self, model):
        try:
            start_time = time.time()
            test_model = model.fit(self.X_train, self.y_train)
            test_score = SCORERS['accuracy'](test_model, self.X_test, self.y_test)
            extra_test_time = time.time() - start_time
        except Exception as e:
            raise Exception("Impossible to learn and try the model on test set {}".format(str(e)))

        return test_score, extra_test_time

    def evaluation(self, config, seed, instance):
        model = self.get_model(config)

        try:
            scores = cross_val_score(model, self.X_train, self.y_train, cv=self.cross_validation, scoring='accuracy')
        except ValueError as e:
            raise Exception('Impossible to learn the model {}'.format(model))

        return 1-np.mean(scores)

    def _call_ta(self, obj, config, **kwargs):
        return obj(config, **kwargs)
