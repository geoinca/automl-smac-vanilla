from typing import Dict
import json
import copy

from ConfigSpace.configuration_space import ConfigurationSpace
from ConfigSpace.hyperparameters import CategoricalHyperparameter
from ConfigSpace.read_and_write.json import read


def get_estimators(search_space_file):
    with open(search_space_file) as json_file:
        raw_spaces = json.load(json_file)

    actual_spaces = spawn_actual_cs(raw_spaces)

    configuration_space = ConfigurationSpace()

    estimators_parameter = CategoricalHyperparameter(
        '__estimator__',
        list(actual_spaces.keys()),
        default_value=list(actual_spaces.keys())[0]
    )

    configuration_space.add_hyperparameter(estimators_parameter)

    for method, method_space in actual_spaces.items():
        parent_hyperparameter = {'parent': estimators_parameter,
                                 'value': method}
        configuration_space.add_configuration_space(
            method,
            method_space,
            parent_hyperparameter=parent_hyperparameter
        )
    return configuration_space, raw_spaces

def spawn_actual_cs(estimators_space):
    actual_cs = copy.deepcopy(estimators_space)
    for base_name, base_estimator in actual_cs.items():
        base_estimator['hyperparameters'] = list(base_estimator['hyperparameters'].values())
        actual_cs[base_name] = read(json.dumps(base_estimator))
    return actual_cs

def get_hyperparameters(configuration_space):
    hyperparameters = dict()
    for k in configuration_space:
        if k == '__estimator__':
            continue
        if configuration_space[k]:
            hyperparameter_name = "".join(k.split(':')[1:])
            hyperparameters[hyperparameter_name] = configuration_space[k]
    return hyperparameters
