import time
import logging

from smac.scenario.scenario import Scenario
from smac.facade.smac_facade import SMAC

from smac.optimizer.objective import average_cost
from smac.runhistory.runhistory import RunHistory

from sklearn.model_selection import train_test_split

from smac.optimizer.smbo import SMBO

from .config_space import get_estimators
from .evaluator import ExecuteTA
from .dataset_manager import get_dataset
from .stats import Stats

"""
    Wrap SMBO in order to catch optimization process's time
"""
class SMBOWrapper(SMBO):
    def run(self):
        Stats().initial_optimization_timer()
        self.start()

        # Main BO loop start from 2nd evaluation
        while True:
            if self.scenario.shared_model:
                self.pSMAC.read(run_history=self.runhistory,
                           output_dirs=self.scenario.input_psmac_dirs,
                           configuration_space=self.config_space,
                           logger=self.logger)

            start_time = time.time()
            X, Y = self.rh2EPM.transform(self.runhistory)

            self.logger.debug("Search for next configuration")
            # get all found configurations sorted according to acq
            challengers = self.choose_next(X, Y)

            time_spent = time.time() - start_time
            time_left = self._get_timebound_for_intensification(time_spent)

            self.logger.debug("Intensify")

            self.incumbent, inc_perf = self.intensifier.intensify(
                challengers=challengers,
                incumbent=self.incumbent,
                run_history=self.runhistory,
                aggregate_func=self.aggregate_func,
                time_bound=max(self.intensifier._min_time, time_left))

            if self.scenario.shared_model:
                self.pSMAC.write(run_history=self.runhistory,
                            output_directory=self.scenario.output_dir_for_this_run)

            logging.debug("Remaining budget: %f (wallclock), %f (ta costs), %f (target runs)" % (
                self.stats.get_remaing_time_budget(),
                self.stats.get_remaining_ta_budget(),
                self.stats.get_remaining_ta_runs()))

            if self.stats.is_budget_exhausted():
                Stats().stop_optimization_timer()
                break

            self.stats.print_stats(debug_out=True)

        return self.incumbent


def optimize(args):
    data, target = get_dataset(args.dataset, args.dataset_mapper_file)

    X_train, X_test, y_train, y_test = train_test_split(data, target, train_size=args.train_size_ratio, test_size=args.test_size_ratio, random_state=args.seed, stratify=target, shuffle=True)

    config_space, raw_config_space = get_estimators(args.search_space)

    runhistory = RunHistory(aggregate_func=average_cost)

    scenario = Scenario({"run_obj": "quality",   # we optimize quality (alternative runtime)
                         "runcount-limit": args.number_of_evaluations,  # maximum number of function evaluations
                         "cs": config_space,               # configuration space
                         "deterministic": "true",
                         "memory_limit": 3072,
                         "cutoff_time": args.cutoff_time,
                         "output_dir": "tmp/",
                         })

    tae_runner = ExecuteTA(raw_config_space = raw_config_space,
    X_train = X_train,
    y_train = y_train,
    X_test = X_test,
    y_test = y_test,
    runhistory = runhistory,
    cross_validation=args.cross_validation)

    smac = SMAC(
        scenario=scenario,
        tae_runner=tae_runner,
        runhistory=runhistory,
        smbo_class=SMBOWrapper,
        rng=args.seed
    )

    incumbent = smac.optimize()
    print(Stats())
    Stats().saveToJson(args.stats_result_file)
