import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sys

def plot_scores(fig, axarr, df_s, test_score):
    number_of_generations = df_s.optimization_times_per_group.apply(lambda x : x.keys()).apply(len).unique()[0]
    score_prefix = "valid_" if not test_score else "test_"

    X = []
    Y = []
    for i in range(0, number_of_generations):
        x = (df_s['optimization_time_group_{}'.format(i)]).mean() / 3600
        y = (df_s['{}score_group_{}'.format(score_prefix, i)]).mean() * 100
        if not test_score:
            y = 100 - y
        X.append(x)
        Y.append(y)
        axarr.plot(x, y, marker='o', color='black', markersize=1, linewidth=0, label="$i^{th}$ group")
        axarr.annotate(str(i), (x,y))

    axarr.plot(X, Y, marker=None, color="blue", alpha=0.7, label="ASV", markersize=1, linewidth=.5)

    infos = "test set" if test_score else "train set"
    axarr.set_title(infos, pad=1)

if __name__ == '__main__':
    input=sys.argv[1]
    output=sys.argv[2]

    df = pd.read_parquet(input, 'fastparquet')

    f, axarr = plt.subplots(1, 2, sharey='row')

    plot_scores(f, axarr[0], df, False)
    plot_scores(f, axarr[1], df, True)

    f.text(0.5, 0.04, 'Elapsed time to get a generation (hours)', ha='center')
    f.text(0.04, 0.5, 'Accuracy average (\%)', va='center', rotation='vertical')

    f.savefig(output)
