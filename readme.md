# AutoML Smac Vanilla

Example of a basic AutoML based on SMAC including metrics that measure the convergence of the optimization process.

`asv` refers to Automl Smac Vanilla.

## How to use ?

Install requirements:
```
apt-get install -y build-essential python3 python3-pip python3-dev git swig
pip3 install -r requirements.txt
python3 -m pip install ConfigSpace==0.4.6 smac==0.8.0
pip3 install .
```
**Note**: If you have `docker`, you can also take a look on `Dockerfile`

Usage:
```
asv --help
```

Example:
```
asv --dataset="iris" --dataset-mapper-file="misc/datasets.yaml" --search-space="misc/search_spaces/classifiers.json"
```
**Note**: large dataset need to be download with `misc/download-dataset.sh`
